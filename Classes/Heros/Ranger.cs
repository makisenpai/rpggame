﻿using System;
using RpgGame.Classes.Helpers;
using RpgGame.Classes.Exceptions;
using static RpgGame.Classes.Enums.Enums;
using RpgGame.Classes.Items;

namespace RpgGame.Classes.Heros
{
    public class Ranger : Hero
    {
        public Ranger() 
        {
            ClassType = "Ranger";
            Name = "Robin the Hood";

            PrimaryStat = new PrimaryStats(1,7,1,8);
            TotalStat = PrimaryStat;
            StatGain = new PrimaryStats(1,5,1,2);
        
        }

        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorType != ArmorTypes.Mail && armor.ArmorType != ArmorTypes.Leather)
            {
                throw new InvalidArmorException($"As a {ClassType} you cannot equip this armor type");
            }
            else if (armor.LevelRequirment > Level)
            {
                throw new InvalidArmorException($"This armor is to high level for your scrub ass {armor.LevelRequirment}");
            }

            gear.Remove(armor.ItemSlots);
            gear.Add(armor.ItemSlots, armor);

            return "new armor equiped :d";

        }
        public override string EquipWeapon(Weapon weapon)
        {

            if (weapon.WeaponType != WeaponTypes.Bow)
            {
                throw new InvalidWeaponException($"As a {ClassType} you cannot equip this type of weapon");
            }
            else if (weapon.LevelRequirment > Level)
            {
                throw new InvalidWeaponException($"This Weapon requiers level {weapon.LevelRequirment}");
            }

            //CODE TO ADD THE WEAPON TO THE WEAPON SLOT IN THE DICITONARY 
            gear.Remove(Slots.weapon);
            gear.Add(Slots.weapon, weapon);

            return "You equiped Dope weapon";

        }
    }
}
