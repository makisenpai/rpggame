﻿using System;
using RpgGame.Classes.Helpers;
using RpgGame.Classes.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgGame.Classes.Items;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame.Classes.Heros
{
    public class Warrior : Hero
    {
        public Warrior()
        {
           
            ClassType = "Warrior";
            Name = "Bobby the Barb";

            PrimaryStat = new PrimaryStats(5, 2, 1, 10);
            TotalStat = PrimaryStat;
            StatGain = new PrimaryStats(3, 2, 1, 5);

        }

        public override string EquipArmor(Armor armor)
        {
            Console.WriteLine(armor.ArmorType);
            if (armor.ArmorType != ArmorTypes.Mail && armor.ArmorType != ArmorTypes.Plate)
            {
                throw new InvalidArmorException($"As a {ClassType} you cannot equip this armor type");
            }
            else if (armor.LevelRequirment > Level)
            {
                throw new InvalidArmorException($"This armor is to high level for your scrub ass {armor.LevelRequirment}");
            }

            gear.Remove(armor.ItemSlots);
            gear.Add(armor.ItemSlots, armor);

            return "new armor equiped :d";

        }
        public override string EquipWeapon(Weapon weapon)
        {

            if (weapon.WeaponType != WeaponTypes.Hammer && weapon.WeaponType != WeaponTypes.Axe && weapon.WeaponType != WeaponTypes.Sword)
            {
                throw new InvalidWeaponException($"As a {ClassType} you cannot equip this type of weapon");
            }
            else if (weapon.LevelRequirment > Level)
            {
                throw new InvalidWeaponException($"This Weapon requiers level {weapon.LevelRequirment}");
            }

            //CODE TO ADD THE WEAPON TO THE WEAPON SLOT IN THE DICITONARY 
            gear.Remove(Slots.weapon);
            gear.Add(Slots.weapon, weapon);

            return "You equiped Dope weapon";

        }
    }
}
