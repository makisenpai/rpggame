﻿using System;
using RpgGame.Classes.Exceptions;
using RpgGame.Classes.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgGame.Classes.Items;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame.Classes.Heros
{
    public class Mage : Hero
    {
       
        public Mage() 
        {
            //Generates a Mage object with mage specifi stats and Name 
           
            ClassType = "Mage";
            Name = "Leah the Witch of New Tristram";

            PrimaryStat = new PrimaryStats(1, 1, 8, 5);
            TotalStat = PrimaryStat;
            StatGain = new PrimaryStats(1, 1, 5, 3);

        }

        public override string EquipWeapon(Weapon weapon)
        {
               
            if (weapon.WeaponType != WeaponTypes.Wand)
                {
                    throw new InvalidWeaponException($"As a {ClassType} you cannot equip this type of weapon");
                } 
            else if (weapon.LevelRequirment > Level)
                {
                    throw new InvalidWeaponException($"This Weapon requiers level {weapon.LevelRequirment}");
                }

             
            gear.Remove(Slots.weapon);
            gear.Add(Slots.weapon, weapon);

            return "You equiped Dope weapon";

        }

        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorType != ArmorTypes.Cloth)
            {
                throw new InvalidArmorException($"As a {ClassType} you cannot equip this armor type");
            }else if (armor.LevelRequirment > Level)
            {
                throw new InvalidArmorException($"This armor is to high level for your scrub ass {armor.LevelRequirment}");
            }

            gear.Remove(armor.ItemSlots);
            gear.Add(armor.ItemSlots, armor);
            gearStat.Add(armor.ItemSlots, armor.ArmorStat);
            return "new armor equiped :d";

        }
    }
}



