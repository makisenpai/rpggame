﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgGame.Classes.Items;
using RpgGame.Classes.Helpers;
using RpgGame.Classes.Enums;
using static RpgGame.Classes.Enums.Enums;


namespace RpgGame.Classes.Heros
{
    public abstract class Hero
    {

        /*
         * Class name : Hero 
         * Abstract class for setting common values between different classes.
         * Contains Objects of Class PrimarStats.
         */
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public string ClassType { get; set; }

        public double Damage { get; set; }
        public PrimaryStats PrimaryStat { get; set; }
        public PrimaryStats TotalStat { get; set; }
        public PrimaryStats StatGain { get; set; }

        public Dictionary<Slots, Items.Items> gear = new();

        public Dictionary<Slots, PrimaryStats> gearStat = new();

        public SecondaryStats SecondaryStat { get; set; }

 

        //Takes primaryStats and adds the stat values of the gear to total value.
        public void UpdateStats()
        {
            PrimaryStats newTotal = PrimaryStat;
            foreach(KeyValuePair<Slots, PrimaryStats> stats in gearStat)
            {
                newTotal.strenght += stats.Value.strenght;
                newTotal.dexterity += stats.Value.dexterity;
                newTotal.intelligence += stats.Value.intelligence;
                newTotal.vitality += stats.Value.vitality;
                //newTotal += new PrimaryStats(stats.Value.strenght, stats.Value.dexterity, stats.Value.intelligence, stats.Value.vitality);
            }
            TotalStat = newTotal;
        }

        //Prints out caracther sheet :MISSING, did not use stringbuilder, was allerde over the time and dident want to late on the hand in.
        public void ShowSheet()
        {
            Console.WriteLine("----------------------------");
            Console.WriteLine($"Name  : { Name }");
            Console.WriteLine($"Class : { ClassType }");
            Console.WriteLine($"Level : { Level }");
            Console.WriteLine($"Stats :");
            Console.WriteLine($"        Strength     : { TotalStat.strenght }");
            Console.WriteLine($"        Dexterity    : { TotalStat.dexterity }");
            Console.WriteLine($"        Intelligence : { TotalStat.intelligence }");
            Console.WriteLine($"        Vitality     : { TotalStat.vitality }");
            Console.WriteLine();
        }

        //Abstract method for checking if armor is usable by the current class. and adding it to gear dicitonary.
        public abstract string EquipArmor(Armor armor);
        //Abstract method for checking if weapon is usable by the current class.
        public abstract string EquipWeapon(Weapon weapon);

        //Calculate dps based on classType. 
        public double CalculateDps()
        {
            Items.Items value;
            if(!gear.TryGetValue(Slots.weapon, out value))
            {
                return 0;
            }
            Weapon weapon = (Weapon)value;
            switch (ClassType)
            {
                case ("Warrior"):
                    Damage = weapon.DPS * (1 + TotalStat.strenght / 100);              
                    break;
                case ("Rouge"):
                    Damage = weapon.DPS * (1 + TotalStat.dexterity / 100);        
                    break;
                case ("Ranger"):
                    Damage = weapon.DPS * (1 + TotalStat.dexterity / 100);       
                    break;
                case ("Mage"):
                    Damage = weapon.DPS * (1 + TotalStat.intelligence / 100);               
                    break;
            }
            return Damage;

        }

        //Adds stat gain's values to primary stat and ++level.
        public void LevelUp()
        {
            Level++;
            PrimaryStat = PrimaryStat + StatGain;
            UpdateStats();
            Console.WriteLine("You have gaind 1 level");
            //Placeholder, Add "add equipment lvl" to TotalStat update.

        }

    }
}
