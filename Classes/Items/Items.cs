﻿using System;
using RpgGame.Classes.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame.Classes.Items
{
    /// <summary>
    /// Class for common values in all items. Parent class of Armor and Weapon.
    /// </summary>
    public abstract class Items
    {
       
        public string ItemName { get; set; }
        public int LevelRequirment { get; set; }
        public Slots ItemSlots { get; set; }

        protected PrimaryStats ItemStats { get; set; }

    }
}
