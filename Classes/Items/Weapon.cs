﻿using System;
using RpgGame.Classes.Enums;
using RpgGame.Classes.Helpers;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame.Classes.Items
{
    public class Weapon : Items
    {
        
        public double WeaponDmg { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
        public WeaponTypes WeaponType { get; set; }

        //Constructor for generating a weapon based on input
        public Weapon(String name, int leveReq,double dmg, double speed, WeaponTypes type) : base()
        {
            ItemName = name;
            LevelRequirment = leveReq;
            ItemSlots = Slots.weapon;
            WeaponDmg = dmg;
            AttackSpeed = speed;
            WeaponType = type;
            DPS = WeaponDmg * AttackSpeed;
            
        }

    }
}
