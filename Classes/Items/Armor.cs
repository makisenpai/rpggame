﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RpgGame.Classes.Enums;
using RpgGame.Classes.Helpers;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame.Classes.Items
{
    public class Armor : Items
    {
      
        public PrimaryStats ArmorStat { get; set; }
        public ArmorTypes ArmorType { get; set; }

        public Armor(String name, int levelReq, Slots slot, ArmorTypes type )
        {
            ItemName = name;
            LevelRequirment = levelReq;
            ItemSlots = slot;
            ArmorType = type;

            PrimaryStats Prim = new PrimaryStats();
            switch (type)
            {

                case (ArmorTypes.Cloth):
                    Prim.strenght = 0;
                    Prim.dexterity = 0;
                    Prim.intelligence = 2;
                    Prim.vitality = 1;
                    break;

                case (ArmorTypes.Leather):
                    Prim.strenght = 0;
                    Prim.dexterity = 2;
                    Prim.intelligence = 0;
                    Prim.vitality = 1;
                    break;

                case (ArmorTypes.Mail):
                    Prim.strenght = 1;
                    Prim.dexterity = 0;
                    Prim.intelligence = 0;
                    Prim.vitality = 2;
                    break;

                case (ArmorTypes.Plate):
                    Prim.strenght = 1;
                    Prim.dexterity = 0;
                    Prim.intelligence = 0;
                    Prim.vitality = 2;
                    break;

            }
            ArmorStat = Prim;


        }
    }
}
