﻿using System;

namespace RpgGame.Classes.Enums
{
    public class Enums
    {
        public enum WeaponTypes
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Sword,
            Wand
        }
        
        public enum ArmorTypes
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public enum Slots
        {
            head,
            body,
            leegs,
            feet,
            weapon,
            ofHand
        }
    }
    
}
