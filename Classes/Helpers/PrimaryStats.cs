﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgGame.Classes.Helpers
{
    public class PrimaryStats
    {
        public int strenght { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }
        public int vitality { get; set; }

        public PrimaryStats()
        {
        }

        public PrimaryStats(int s, int d, int i, int v)
        {
            strenght = s;
            dexterity = d;
            intelligence = i;
            vitality = v;
        }

        public static PrimaryStats operator +(PrimaryStats lhs, PrimaryStats rhs)
        {
            return new PrimaryStats
            {
                strenght = lhs.strenght + rhs.strenght,
                dexterity = lhs.dexterity + rhs.dexterity,
                intelligence = lhs.intelligence + rhs.intelligence,
                vitality = lhs.vitality + rhs.vitality
            };
        }

        public override bool Equals(object obj)
        {
            return obj is PrimaryStats stats &&
                   strenght == stats.strenght &&
                   dexterity == stats.dexterity &&
                   intelligence == stats.intelligence &&
                   vitality == stats.vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(strenght, dexterity, intelligence, vitality);
        }
    }
}
