﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgGame.Classes.Helpers
{
    public class SecondaryStats
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public SecondaryStats(PrimaryStats total)
        {
            ArmorRating = total.strenght + total.dexterity;
            Health = total.vitality * 10;
            ElementalResistance = total.intelligence;
        }
        
    }
}
