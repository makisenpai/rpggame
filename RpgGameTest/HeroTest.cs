using System;
using Xunit;
using RpgGame.Classes.Heros;
using RpgGame.Classes.Helpers;

namespace RpgGameTest
{
    public class HeroTests
    {
        [Fact]
        public void NewCharacter_NewCharacterIsLevelOne_ShouldBeTrue()
        {
            
            Mage testMage = new Mage();

            int expected = 1;

            Assert.Equal(expected, testMage.Level);
        }

        [Fact]
        public void NewCharacter_NewCharacterIsLevelTwo_ShouldBeTrue()
        {

            Mage testMage = new Mage();
            testMage.LevelUp();

            int expected = 2;

            Assert.Equal(expected, testMage.Level);
        }

        [Fact]
        public void Mage_MageBaseStatsIsCorrect_ShouldBeTrue()
        {

            Mage testMage = new Mage();

            PrimaryStats prim = new()
            {
                strenght = 1,
                dexterity = 1,
                intelligence = 8,
                vitality = 5
            };

            Assert.Equal(prim, testMage.PrimaryStat);
        }

        [Fact]
        public void Warrior_WarriorBaseStatsIsCorrect_ShouldBeTrue()
        {

            Warrior testWarrior = new Warrior();

            PrimaryStats prim = new()
            {
                strenght = 5,
                dexterity = 2,
                intelligence = 1,
                vitality = 10
            };

            Assert.Equal(prim, testWarrior.PrimaryStat);
        }

        [Fact]
        public void Ranger_RangerBaseStatsIsCorrect_ShouldBeTrue()
        {

            Ranger testRanger = new Ranger();

            PrimaryStats prim = new()
            {
                strenght = 1,
                dexterity = 7,
                intelligence = 1,
                vitality = 8
            };

            Assert.Equal(prim, testRanger.PrimaryStat);
        }

        [Fact]
        public void Rouge_RougeBaseStatsIsCorrect_ShouldBeTrue()
        {

            Rouge testRouge = new Rouge();

            PrimaryStats prim = new()
            {
                strenght = 2,
                dexterity = 6,
                intelligence = 1,
                vitality = 8
            };

            Assert.Equal(prim, testRouge.PrimaryStat);
        }

    }
}
