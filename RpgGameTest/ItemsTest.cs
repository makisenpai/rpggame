﻿using System;
using Xunit;
using RpgGame.Classes.Heros;
using RpgGame.Classes.Items;
using RpgGame.Classes.Exceptions;
using static RpgGame.Classes.Enums.Enums;


namespace RpgGameTest
{
    public class ItemsTest
    {
        Weapon TestWand1 = new("Boom Stick", 1, 7, 1.1, WeaponTypes.Wand);
        Weapon TestWand2 = new("BoomBoom Stick", 2, 7, 1.1, WeaponTypes.Wand);
        Armor TestArmorClothBody1 = new("Coat Of Magi", 1, Slots.body, ArmorTypes.Cloth);
        Armor TestArmorClothBody2 = new("Coat Of MagiGgigigig", 2, Slots.body, ArmorTypes.Cloth);

        [Fact]
        public void Weapon_WeaponIsToHighLevel_ThrowInvalidWeaponException()
        {
            Mage testMage = new();

            Assert.Throws<InvalidWeaponException>(() => testMage.EquipWeapon(TestWand2));
        }

        [Fact]
        public void Weapon_WeaponIsWrongType_ThrowInvalidWeaponException()
        {
            Warrior testWarrior = new();

            //Tests if warrior can equip a wand. Should throw and expection.
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(TestWand1));
        }

        [Fact]
        public void Armor_ArmorIsToHighLevel_ThrowInvalidArmorException()
        {
            Mage testMage = new();

            Assert.Throws<InvalidArmorException>(() => testMage.EquipArmor(TestArmorClothBody2));
           
        }

        //I DONT KNOW WHY THIS FAILS, The debugger says that the EquipArmor throws a ex, but this test wont catch it. :(
        [Fact]
        public void Armor_ArmorIsWrongType_ThrowInvalidArmorException()
        {
            Mage testWarrior = new();

            Action expected = () => testWarrior.EquipArmor(TestArmorClothBody1);

            Assert.Throws<InvalidArmorException>(expected);

        }

        [Fact]
        public void Weapon_ValidWeaponEquiped_ReturnString()
        {
            Mage testMage = new();

            string Expected = "You equiped Dope weapon";


            Assert.Equal(Expected, testMage.EquipWeapon(TestWand1));
        }

        [Fact]
        public void Armor_ValidArmorEquiped_ReturnString()
        {
            Mage testMage = new();

            string Expected = "new armor equiped :d";


            Assert.Equal(Expected, testMage.EquipArmor(TestArmorClothBody1));
        }

        [Fact]
        public void Weapon_CheckIfDps_ShouldBeTrue()
        {
            //This one works, but if i change the intelligence stat, it stil works so am prty sure somthing here is wrong, but i cant figuer it out.
            Mage testMage = new();
            testMage.EquipWeapon(TestWand1);
            double expected = (7 * 1.1) * (1 * (8 / 100));

            Assert.Equal(expected, testMage.Damage);
        }

        [Fact]
        public void Weapon_CheckIfDpsWithWepAndGear_ShouldBeTrue()
        {
            //Same problom as test over.
            Mage testMage = new();
            testMage.EquipArmor(TestArmorClothBody1);
            testMage.EquipWeapon(TestWand1);
            double expected = (7 * 1.1) * (1 * (9 / 100));

            Assert.Equal(expected, testMage.Damage);
        }

    }
}
