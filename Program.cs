﻿using System;
using RpgGame.Classes.Heros;
using RpgGame.Classes.Items;
using static RpgGame.Classes.Enums.Enums;

namespace RpgGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Armor TestArmorClothBody1 = new("Coat Of Magi", 1, Slots.body, ArmorTypes.Cloth);
            Armor TestArmorClothBody2 = new("Coat Of MagiGgigigig", 2, Slots.body, ArmorTypes.Cloth);
            Warrior warrior = new();
            warrior.EquipArmor(TestArmorClothBody1);
            
            warrior.ShowSheet();
        }
    }
}
